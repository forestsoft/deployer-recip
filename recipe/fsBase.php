<?php
namespace Deployer;

use Deployer\Host\Localhost;
use Deployer\Task\Context;
use Forestsoft\Deployer\Factory;
require_once 'recipe/common.php';

inventory("hosts.yml");

set('current_dir', function() {
    return run('pwd');
});

task("init", function() {
    $instance = Factory::getInstance();
    $instance->init();

})->once()->setPrivate(true);

after("deploy:prepare", "init");

function parseAppConfig($settingsApp, $index = null) {
    foreach ($settingsApp as $key => $value) {
        if (is_array($value) && preg_match("#[A-z]+#", $key)) {
            parseAppConfig($value, $index . "." . $key );
        } else {
            $indexKey = $index . "." . $key;

            if (isDebug()) {
                writeln("Set config " . $indexKey . " to '" . $value . "'");
            }

            set($indexKey, $value);
        }
    }
}

function runSuCommand($command) {
    if (get('user') == "root" || stristr($command, "sudo")) {
        run($command);
    } else {
        writeln("<comment>" . sprintf("Cannot run command as User \"" . get("user") . "\". Please execute manually:") . "</comment>");
        writeln($command);
    }
}

/**
 * @deprecated should use over Configuration File Writer in Deployer lib
 */
task('deploy:configure', function() {
    $configFilesToReplace = get("processConfig");

    $targets = [];
    foreach($configFilesToReplace as $idx => $value) {
        $parts = explode(":", $value);
        $targets[$parts[0]] = (!empty($parts[1])) ? $parts[1] : $parts[0];
    }

    $host = Context::get()->getHost();

    $tmpDir = sys_get_temp_dir();
    /* @var $file SplFileInfo */
    foreach ($targets as $file => $target) {
        $tmpFileDl = tempnam($tmpDir, 'tmp');
        if (!($host instanceof Localhost)) {
             download("{{release_path}}/" . $file, $tmpFileDl);
        } else {
            $tmpFileDl = parse("{{release_path}}/$file");
        }

        $success = false;
        // Make tmp file
        $tmpFile = tempnam($tmpDir, 'tmp');
        if (!empty($tmpFile)) {
            try {
                //$contents = $compiler(file_get_contents($file->getPathname()));

                if (!file_exists($tmpFileDl)) {
                    throw new \InvalidArgumentException(sprintf("Something went wrong: Configfile '%s' does not exists to replace variables", $tmpFileDl));
                }

                $contents = parse(file_get_contents($tmpFileDl));
                $target = parse($target);
                if (substr($target, 0, 1) !== "/") {
                    $target = get("release_path") . "/" . $target;
                }
                // Put contents and upload tmp file to server
                if (file_put_contents($tmpFile, $contents) > 0) {
                    upload($tmpFile, $target);
                    $success = true;
                }
            } catch (\Exception $e) {
                throw $e;
            }
            // Delete tmp file
            unlink($tmpFileDl);
            unlink($tmpFile);
        }
        if ($success) {
            writeln(sprintf("<info>✔</info> %s", $file));
        } else {
            writeln(sprintf("<fg=red>✘</fg=red> %s", $file));
        }
    }
});

task('read-config', function() {
    $settingsApp = get("app");
    parseAppConfig($settingsApp, "app");
    if (isDebug()) {
        var_dump(Context::get()->getConfig());
    }
});
before("deploy:configure", "read-config");

task('deploy:compile_static', function() {
    $settingsApp = get("app");
    $cssToCompile = $settingsApp["compile"]["css"];

    foreach($cssToCompile as $file) {
        $paths = explode(":", $file);
        run("scss {{release_path}}/$paths[0] {{release_path}}/$paths[1]");

        if (stristr($paths[1], ".min.css")) {
            $dstFile = "/tmp/".basename($paths[1]);
            download("{{release_path}}/$paths[1]", $dstFile);
            runLocally("yui-compressor $dstFile -o $dstFile");
            upload($dstFile, "{{release_path}}/$paths[1]");
            unlink($dstFile);
        }
    }
});

task('deploy:compile_static_local', function() {
    $settingsApp = get("app");
    $cssToCompile = $settingsApp["compile"]["css"];

    $tmpFolder = sys_get_temp_dir() . "/" . get("hostname");
    @mkdir($tmpFolder);
    $compilerFiles[] = array();
    $x = 0;
    foreach($cssToCompile as $file) {
        $paths = explode(":", $file);
        $fName = $tmpFolder . "/" . basename($paths[0]);
        download("{{release_path}}/" . $paths[0], $fName);

        $compilerFiles[$x]['compile'] = $fName;
        $compilerFiles[$x]['source'] = $paths[0];

        if (!empty($paths[1])) {
           $compilerFiles[$x]['dst'] = $paths[1];
        } 

        $x++;
    }

    foreach($compilerFiles as $k => $v) {
        if (empty($v["dst"])) {
            continue;
        }
        runLocally("scss " . $v["compile"] ." " . dirname($v["compile"]) . "/" . basename($v["dst"]));
        if (stristr($v["dst"], ".min.css")) {
            runLocally("yui-compressor " . dirname($v["compile"]) . "/" . basename($v["dst"]) ." -o " . dirname($v["compile"]) . "/" . basename($v["dst"]));
            upload(dirname($v["compile"]) . "/" . basename($v["dst"]), "{{release_path}}/" . $v["dst"]);
            unlink(dirname($v["compile"]) . "/" . basename($v["dst"]));
        }
    }
    runLocally( "rm -Rf " . $tmpFolder);
});

task('tag', function () {
    $version = "deployment-" . get('hostname') . "-" . date("Y-m-d\TH-i-s");
    run("cd {{release_path}} && git tag -a $version -m \"Deployment " . get("hostname") . "\" " . " && git push origin $version");
});

task('reload-services', function () {
    $config = get("app");
    if (!empty($config['reload_services'])) {
        foreach ($config['reload_services'] as $serviceCommand) {
            if (!empty($serviceCommand)) {
                runSuCommand($serviceCommand);
                writeln(sprintf("<info>✔</info> Run command: <fg=green>%s</>", $serviceCommand));
            }
        }
    } else {
        writeln(sprintf("<fg=red>✘</fg=red> <comment>There are no services under app.reload_services defined to reload</comment>"));
    }
});


function runDatabaseCommand($command) {
    Factory::getInstance()->getDatabaseCommand()->run($command);
}