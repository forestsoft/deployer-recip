<?php

use function Deployer\ask;
use function Deployer\askChoice;
use Deployer\Deployer;
use function Deployer\runLocally;
use function Deployer\task;
use function Deployer\writeln;

task('mage:update:core', function () {
   $wd = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid("mage_update");
   mkdir($wd);

   $remoteRepo = ask("Enter URL to remote Repo for Update", 'ssh://forestsoft@bitbucket.org/hifischluderbacher/webschop.git');
   $remoteBranch = ask("Enter branch as source", "development");

   $updatePackageUrl = askChoice("Enter the URL to the Magento Update Package", [
      1 => 'http://forestsoft.fritz.box/updates/magento1/magento-1.9.3.8-2018-02-23-06-06-07.tar.bz2',
      2 => 'http://forestsoft.fritz.box/updates/magento1/magento-1.9.3.10-2018-09-18-03-21-10.tar.bz2'
   ], 2);
   $packageRelativePathToMage = ask("Enter Relative Path in Update Package", "/magento/");

   $cloneTarget = $wd . DIRECTORY_SEPARATOR . "_remote_project";
   mkdir($cloneTarget);
   
   writeln(sprintf("<info>Clone %s into %s and checkout branch </info>", $remoteRepo, $cloneTarget, $remoteBranch));
   runLocally("git clone --recursive " . $remoteRepo . " " . $cloneTarget . " && cd " . $cloneTarget . " && git checkout origin/" . $remoteBranch);



   $localUpdateFile = $wd . DIRECTORY_SEPARATOR . basename($updatePackageUrl);
   writeln("<info>Download $updatePackageUrl</info>");
   runLocally("wget -O " . $localUpdateFile . " " . $updatePackageUrl);
   writeln("<info>Done</info>");
   writeln("<info>Unpack $localUpdateFile</info>");
   unpackArchive($localUpdateFile);

   writeln(sprintf("<info>Make Branch %s</info>", "mage-update"));
   runLocally("cd $cloneTarget && git checkout -b mage-update");
   $src = dirname($localUpdateFile) . "/" . $packageRelativePathToMage . "/* ";
   $dst = $cloneTarget . "/src";

   cleanupMageCore($dst);
   writeln(sprintf("<info>Copy Artifacts from %s to %s </info>", $src, $dst));
   runLocally("cp -Rf " . $src . " " . $dst);



   writeln("<info>Commit to Branch</info>");
   runLocally("cd $cloneTarget && git add . && git commit -a -m\"Update Magento by $updatePackageUrl\"");

   writeln("<info>Done</info>");

   runLocally("cd $cloneTarget && git push -u origin mage-update");
  //unlink($wd);
});

function cleanUpMageCore($dst) {
    if (!empty($dst) && $dst !== "/") {
        runLocally("rm -Rf " . $dst . "/app/code/core");
        runLocally("rm -Rf " . $dst . "/lib/Zend");
        runLocally("rm -Rf " . $dst . "/js/mage");
        runLocally("rm -Rf " . $dst . "/js/prototype");
    }
}

function unpackArchive($file, $target = null) {
    if ($target == null) {
        $target = dirname($file);
    }
    $options = "x";
    if ( stristr($file, ".tar")) {
        if (stristr($file, ".tar.bz2")) {
            $options .= "j";
        } elseif (stristr($file, ".tar.gz")) {
            $options .= "z";
        }
        runLocally("tar " . $options . "f " . $file . " -C " . $target);
    } elseif (stristr($file, ".zip")) {
        runLocally("unzip " . $file . " -d " . $target);
    }
}